from django.apps import AppConfig


class ProenergyAppConfig(AppConfig):
    name = 'proenergy_app'
