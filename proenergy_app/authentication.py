from rest_framework.authtoken.models import Token

def token_active(token):
    if not token: return False
    try:
        #print(Token.objects.get(key = token))
        tokenIn = Token.objects.get(key = token)
        #print(tokenIn)
        return True
    except Token.DoesNotExist:
        return False

def isAdmin(token=None, request = None):
    #print("IS ADMIN")
    if(not token): token = request.META.get('HTTP_TOKEN')
    #print(request.META.get('HTTP_TOKEN'))
    if token_active(token):
        #print(Token.objects.get(key=token).user.is_superuser)
        return Token.objects.get(key=token).user.is_superuser

    return False
