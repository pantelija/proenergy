
from .utils import *
from .authentication import *
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework.views import exception_handler


class ExceptionHandlerMiddleware(object):

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_exception(self,request, exception):
        response = generate_response(exception.__str__(), status.HTTP_500_INTERNAL_SERVER_ERROR)
        return response

class TokenAndAdminMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        response = self.get_response(request)
        return response


    def _generate_401_unauthorized(self, message = "Nemate privilegije za ovu akciju."):
        return generate_response(message, status.HTTP_401_UNAUTHORIZED)

    def process_view(self, request, view_func, *view_args, **view_kargs):

        #print(getURL(request))
        currentURL = getURL(request)
        print(currentURL)
        if currentURL.startswith('login'):
            return
        token = request.META.get('HTTP_TOKEN')
        if not token_active(token):
            return self._generate_401_unauthorized("Morate biti prijavljeni da bi pristupili resursu.")
        # Check authorization if not a nonAuthURL
