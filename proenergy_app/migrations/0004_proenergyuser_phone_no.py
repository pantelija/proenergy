# Generated by Django 2.0.9 on 2019-05-25 12:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('proenergy_app', '0003_proenergyuser_role'),
    ]

    operations = [
        migrations.AddField(
            model_name='proenergyuser',
            name='phone_no',
            field=models.CharField(default='+381601234567', max_length=16),
        ),
    ]
