# Generated by Django 2.0.9 on 2019-05-25 17:50

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('proenergy_app', '0012_auto_20190525_1712'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('payer', models.CharField(max_length=200)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('amount', models.FloatField(default=0.0, validators=[django.core.validators.MinValueValidator(0.0)])),
                ('purpose', models.CharField(max_length=500)),
                ('payment_flag', models.BooleanField(default=False)),
                ('project', models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='proenergy_app.Project')),
            ],
        ),
        migrations.RemoveField(
            model_name='workorder',
            name='working_hours',
        ),
    ]
