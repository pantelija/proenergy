from django.db import models
from django.utils import timezone
# Create your models here.
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator

class Role(models.Model):
    role_name = models.CharField(unique = True, blank = False, max_length = 20)



class ProenergyUser(AbstractUser):

    #role = models.ForeignKey('Role', on_delete=models.CASCADE, default = 1)
    phone_no = models.CharField(blank = False, max_length = 16, null = False, default = '+381601234567')


class Worker(models.Model):

    first_name = models.CharField(null = False, blank = False, max_length = 30, default = 'nepoznato')
    last_name = models.CharField(null = False, blank = False, max_length = 30, default = 'nepoznato')
    email = models.EmailField(unique = True)
    phone_no = models.CharField(null = False, blank = False, max_length = 30, default = 'nepoznato')
    vocation = models.CharField(null = False, blank = False, max_length = 30, default = 'nepoznato')
    duty = models.CharField(null = False, blank = False, max_length = 30, default = 'nepoznato')
    salary_unit = models.FloatField(null = False, blank = False, default = 1.0)


class Project(models.Model):
    project_name = models.CharField(null = False, unique = True, blank = False, max_length = 30)
    project_budget = models.FloatField(null = False, blank = False, default = 1.0, validators=[MinValueValidator(0.0)])
    project_leader = models.ForeignKey('ProenergyUser', on_delete = models.SET_NULL, null = True)
    date_created = models.DateField(auto_now_add = True)
    project_costs = models.FloatField(default = 0.0, validators=[MinValueValidator(0.0)])
    project_income = models.FloatField(default = 0.0, validators=[MinValueValidator(0.0)])
    date_closed = models.DateField(null = True)
    active = models. BooleanField(default = True)


class WorkOrder(models.Model):
    worker = models.ForeignKey('Worker', on_delete = models.SET_NULL, null = True)
    project = models.ForeignKey('Project', on_delete = models.CASCADE, default = 1)
    task = models.CharField(max_length = 2000)
    time_opened = models.DateTimeField(auto_now_add = True)
    time_closed = models.DateTimeField(default = None, null = True)
    #working_hours = models.PositiveIntegerField(default = 0)

class CostType(models.Model):
    name = models.CharField(max_length = 50)


class Payment(models.Model):
    project = models.ForeignKey('Project', on_delete = models.CASCADE, default = None, null = True)
    payer = models.CharField(max_length = 200)
    date = models.DateField(auto_now_add = True)
    amount = models.FloatField(default = 0.0, validators=[MinValueValidator(0.0)])
    purpose = models.CharField(max_length = 500)
    #False - isplate, True - uplate
    payment_flag = models.BooleanField(default = False)
    cost_type = models.ForeignKey('CostType', on_delete = models.CASCADE, default = None, null = True)

class WorkOrderReport(models.Model):
    hours = models.PositiveSmallIntegerField(default = 0)
    work_order = models.ForeignKey('WorkOrder', on_delete = models.CASCADE, default = None, null = True)
    comment = models.CharField(max_length = 1000, blank = True, null = True)
    date = models.DateField(auto_now_add = True)
