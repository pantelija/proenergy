
from rest_framework import serializers
from proenergy_app.models import *
from django.contrib.auth.models import User
#from ninth_gate.utils import getCustomUser

from rest_framework import serializers
from django.contrib.auth import authenticate
from rest_framework import exceptions
import proenergy_app.views


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProenergyUser
        fields = '__all__'

    def to_representation(self, object):
        return {
                "id":object.id,
                "username":object.username,
                "first_name": object.first_name,
                "last_name": object.last_name,
                "email": object.email,
                "phone_no": object.phone_no,


        }

class LoginSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        email = data.get("email", "")
        password = data.get("password", "")
        if email and password:
            user = ProenergyUser.objects.get(email=email, password=password)
            if user:
                data["user"] = user
            else:
                msg = "Nije moguce prijaviti se na sistem sa ovim podacima."
                raise exceptions.ValidationError(msg)
        else:
            msg = "Morate da unesete i email i password."
            raise exceptions.ValidationError(msg)
        return data


class WorkerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Worker
        fields = '__all__'

class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = '__all__'

    def to_representation(self, object):
        #is_str = lambda x: type(x) == str

        def date_repr(date):
            if(type(date) != str):
                return date.strftime("%d/%m/%Y") if object.date_closed else None
            else:
                return '/'.join(reversed(date.split('-')))
        print(type(object.date_closed))
        return {
                "id":object.id,
                "project_name":object.project_name,
                "project_leader": object.project_leader.first_name + ' ' + object.project_leader.last_name,
                "date_created": object.date_created.strftime("%d/%m/%Y") if object.date_created else None,
                "date_closed": date_repr(object.date_closed),#.strftime("%d/%m/%Y") if object.date_closed else None,
                "project_budget": object.project_budget,
                "project_costs": object.project_costs,
                "active":object.active

        }


class WorkOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkOrder
        fields = '__all__'

    def to_representation(self, object):
        time_opened = object.time_opened.strftime("%d/%m/%Y, %H:%M:%S") if \
        object.time_opened else None

        time_closed = object.time_closed.strftime("%d/%m/%Y, %H:%M:%S") if \
        object.time_closed else None

        return {
                 "id": object.id,
                 "task": object.task,
                 "time_opened": time_opened,
                 "time_closed": time_closed,
                 "working_hours": sum([wr.hours for wr in object.workorderreport_set.all()]),
                  "worker": object.worker.first_name + ' ' + object.worker.last_name if object.worker else None,
                  "project": '{} - {}'.format(object.project.id, object.project.project_name)


        }


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'

    def to_representation(self, object):
        return {
                 "id": object.id,
                 "payer": object.payer,
                 "date": object.date.strftime("%d/%m/%Y") if object.date else None,
                 "amount": object.amount,
                 "purpose": object.purpose,
                  "payment_flag": "uplata" if object.payment_flag else "isplata",
                  "project": '{} - {}'.format(object.project.id, object.project.project_name),
                  'cost_type':object.cost_type.name if object.cost_type else None

        }

class WorkOrderReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkOrderReport
        fields = '__all__'


    def to_representation(self, object):
        worker_name = object.work_order.worker.first_name + ' ' +  \
        object.work_order.worker.last_name if object.work_order.worker else None
        project = object.work_order.project if object.work_order.project else None
        #project_data = '{} - {}'.format(object.work_order.project.id, object.work_order.project.
        return {
                "id":object.id,
                 "work_order": object.work_order.id,
                 "worker":worker_name,
                 "hours": object.hours,
                  "project": '{} - {}'.format(project.id, project.project_name),
                  "comment":object.comment,
                  "date":object.date.strftime("%d/%m/%Y") if object.date else None

        }


class CostTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CostType
        fields = '__all__'
    #def to_representation(self, object):
