

import os, re, xlrd
from proenergy_app.models import WorkOrder, WorkOrderReport, Project, Payment, CostType

class ExcelService:


    @staticmethod
    def import_from_excel(flag):

        if(flag == 1):
            return ExcelService.import_work_orders()
        else:
            return ExcelService.import_payments()

    @staticmethod
    def import_work_orders():

        filename = 'resources/radni_nalozi.xls'
        book = xlrd.open_workbook(filename)

        data_sheet = book.sheet_by_index(0)
        header = data_sheet.row_values(0)
        i = 0
        flag = True

        while i < data_sheet.nrows:
            i += 1
            try:
                row = data_sheet.row_values(i)
            except:
                break
            working_order = int(row[0])
            hours = int(row[1])
            date = row[2]
            comment = row[3]
            print(working_order)
            #print('usao ovde')
            try:
                work_order_object = WorkOrder.objects.get(id = working_order)
                print(work_order_object)
            except Exception as e:
                flag = False
                print(e)
                continue

            try:
                WorkOrderReport.objects.create(work_order = work_order_object,\
                comment = comment, date = date, hours = hours)
                print('Uspesno upisan izvestaj')
            except Exception as e:
                print(e)
                flag = False
                continue


        return flag


    @staticmethod
    def import_payments():

        filename = 'resources/placanje.xls'
        book = xlrd.open_workbook(filename)

        data_sheet = book.sheet_by_index(0)
        header = data_sheet.row_values(0)
        i = 0
        flag = True

        while i < data_sheet.nrows:
            i += 1
            try:
                row = data_sheet.row_values(i)
            except:
                break
            row = data_sheet.row_values(i)
            project = int(row[0])
            payer = row[1]
            purpose = row[2]
            cost_type = row[3]
            payment_flag = False if row[4] == 'I' else True
            amount = row[5]
            date = row[6]

            #print('usao ovde')
            try:
                project_object = Project.objects.get(id = project)
                if(cost_type != 'Uplata'):costtype_object = CostType.objects.get(name = cost_type)
                else: costtype = None
            except Exception as e:
                flag = False
                print(e)
                continue

            try:
                Payment.objects.create(project = project_object,\
                payer = payer, date = date, amount = amount, cost_type = costtype_object,\
                purpose = purpose, payment_flag = payment_flag)
                print('Uspesno upisan izvestaj')
            except Exception as e:
                print(e)
                flag = False
                continue


        return flag
