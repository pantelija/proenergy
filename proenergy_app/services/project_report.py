from proenergy_app.models import *
from django.db.models import Sum, F, Count

from datetime import datetime


class ProjectReport:

    def __init__(self, project, worker):

        self._project = project
        self._worker = worker


    def getProject(self):
        return self._project


    def setProject(self, project):
        self._project = project

    def getWorker(self):
        return self._worker

    def setWorker(self, worker):
        self._worker = worker


    def projectReport(self):
        resp = {}
        if(self.getWorker() is None and self.getProject() is not None):

            resp['budget'] = self.getProject().project_budget
            costs = Payment.objects.filter(project = self.getProject(), payment_flag = False).aggregate(costs = Sum('amount'))['costs']
            income = Payment.objects.filter(project = self.getProject(), payment_flag = True).aggregate(income = Sum('amount'))['income']
            #if(costs):
            resp['costs'] = costs if costs else 0
            resp['income'] = income if income else 0
            #print(income)
            resp['profit'] = resp['income'] - resp['costs']
            resp['percentage_of_budget'] = round(resp['costs'] / resp['budget'] * 100,2) if(resp['budget'] != 0) else None

            cost_groups = Payment.objects.filter(project =  self.getProject(), \
            payment_flag = False).values('cost_type__name').annotate(dcount=Sum('amount'))
            resp['cost_groups'] = cost_groups
        elif(self.getWorker() is not None):

            resp['worker_name'] = self.getWorker().first_name + ' ' + self.getWorker().last_name
            resp['worker_id'] = self.getWorker().id
            resp['price'] = self.getWorker().salary_unit

            if(self.getProject() is not None):
                total_hours = WorkOrderReport.objects.filter(work_order__in =  \
                WorkOrder.objects.filter(project = self.getProject(), worker = \
                self.getWorker(),time_closed = None)).values('hours').\
                annotate(sum=Sum('hours'))\
                .values('sum')
                if(total_hours.count() == 0) : resp['total_hours'] = 'Nema radnih \
                aktivnosti koje odgovoraju zadatom upitu.'
                else: resp['total_hours'] = total_hours[0]['sum']
            #print(total_hours)
            else:

                total_hours = WorkOrderReport.objects.filter(work_order__in =  \
                WorkOrder.objects.filter(worker = \
                self.getWorker(),time_closed = None)).values('hours').\
                annotate(sum=Sum('hours'))\
                .values('sum')
                if(total_hours.count() == 0) :
                    resp['total_hours'] = 'Nema radnih aktivnosti koje odgovoraju zadatom upitu.'
                else: resp['total_hours'] = total_hours[0]['sum']
            try:
                resp['total_price'] = resp['total_hours'] * resp['price']
            except:
                resp['total_price'] = None
        else:
            return {'error':'Morate zadati projekat i/ili radnika'}

        return resp

    @staticmethod
    def periodicalReport(period):
        #print(period)
        if int(period) not in (3,6,12):
            #print('ovde')
            return {'error':'Nije moguće generisati izveštaj u ovom periodu.'}
        current_year = str(timezone.now().year)
        dates_dict = {3:[(datetime.strptime('01-01-' + current_year,'%d-%m-%Y'),\
        datetime.strptime('31-03-' + current_year,'%d-%m-%Y')),\
        (datetime.strptime('01-04-' + current_year,'%d-%m-%Y'), \
        datetime.strptime('30-06-' + current_year,'%d-%m-%Y')),\
        (datetime.strptime('01-07-' + current_year,'%d-%m-%Y'), \
        datetime.strptime('30-09-' + current_year,'%d-%m-%Y')),\
        (datetime.strptime('01-10-' + current_year,'%d-%m-%Y'), \
        datetime.strptime('31-12-' + current_year,'%d-%m-%Y'))],

        6:[(datetime.strptime('01-01-' + current_year,'%d-%m-%Y'),\
        datetime.strptime('30-06-' + current_year,'%d-%m-%Y')),\
        (datetime.strptime('01-07-' + current_year,'%d-%m-%Y'),\
        datetime.strptime('31-12-' + current_year,'%d-%m-%Y'))],

        12:[(datetime.strptime('01-01-' + current_year,'%d-%m-%Y'),\
        datetime.strptime('31-12-' + current_year,'%d-%m-%Y'))]}

        #starting_date = datetime.strptime('01-01-' + current_year,'%d-%m-%Y')
        #ending_date = datetime.strptime('01-06-' + current_year,'%d-%m-%Y')

        result = {}
        count = 0
        for dates in dates_dict[period]:
            count += 1
            result[count] = ProjectReport.timeReport(dates[0], dates[1])
        return result

    @staticmethod
    def timeReport(starting_date=None, ending_date=None):

        month_num_to_name = {1:'Januar', 3 : 'Mart',\
        4:'April', 6 : 'Jun', 7:'Jul', 9 : 'Septembar',\
        10:'Oktobar', 12 : 'Decembar'}
        costs = Payment.objects.filter(date__gte= starting_date, date__lte=ending_date, \
        payment_flag = False).values('cost_type__name').annotate(sum = Sum('amount'))
        #print('usao ovde')

        income = Payment.objects.filter(date__gte= starting_date, date__lte=ending_date, \
        payment_flag = True).values('cost_type__name').annotate(sum = Sum('amount'))
        return {'starting_period':month_num_to_name[starting_date.month], \
        'ending_date':month_num_to_name[ending_date.month], 'costs':costs, 'income':income,\
        'total':sum([val['sum'] for val in costs])}
