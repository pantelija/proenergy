from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from proenergy_app import views
from django.urls import path



router = DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'workers', views.WorkerViewSet)
router.register(r'projects', views.ProjectViewSet)
router.register(r'work-orders', views.WorkOrderViewSet)
router.register(r'payments', views.PaymentViewSet)
router.register(r'work-orders-reports', views.WorkOrderReportViewSet)
router.register(r'cost-types', views.CostTypeViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
    path('login/', views.LoginView.as_view(), name="user_login"),
    path('logout/', views.LogoutView.as_view(), name="user_logout"),
    path('work-orders/<int:id>/close/', views.WorkOrderViewSet.as_view({"post": "closeWorkOrder"})),
    path('reports/', views.ReportViewSet.as_view(), name = 'reports'),
    path('excel/', views.ExcelReportView.as_view(), name = 'excel'),
    ]
