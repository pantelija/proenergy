
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from django.http import HttpResponse


def getURL(request):
    nonHostURL = ''
    pathSeparator = request.build_absolute_uri().split('/')
    #print(pathSeparator)
    for elem in pathSeparator[3:]:
        if elem != '':
            nonHostURL += elem + '/'
    return nonHostURL

def getURI(request):
    return request.build_absolute_uri()

def rootPattern(url, rootList):
    urlPath = url.split('/')
    print(urlPath)
    for root in rootList:
        print(root)
        if root[:-1] == urlPath[0]:
            return True
    return False


    
def generate_response(message, status):
    response = Response(
        {"message": message},
        content_type="application/json",
        status=status,
    )
    response.accepted_renderer = JSONRenderer()
    response.accepted_media_type = "application/json"
    response.renderer_context = {}

    return response
