from django.shortcuts import render, get_object_or_404

# Create your views here.
from rest_framework import viewsets, mixins
from rest_framework import serializers
from proenergy_app.serializers import *
from proenergy_app.models import *
from proenergy_app.services.project_report import ProjectReport
from proenergy_app.services.excel_service import ExcelService
from rest_framework.response import Response

from rest_framework.views import APIView
from django.contrib.auth import login as django_login, logout as django_logout
from rest_framework.authtoken.models import Token
from rest_framework.authentication import TokenAuthentication
#from .serializers import LoginSerializer
from rest_framework.decorators import action



from rest_framework.renderers import JSONRenderer

class UserViewSet(viewsets.ModelViewSet):

    queryset = ProenergyUser.objects.all()
    serializer_class = UserSerializer
    authentication_classes = (TokenAuthentication,)
    lookup_field = 'id'



class LoginView(APIView):
    authentication_classes = (TokenAuthentication,)
    def post(self, request):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        try:
            userObject = ProenergyUser.objects.get(email=user.email, password=user.password)
            #role = userObject.is_superuser
            username = userObject.username
            django_login(request, user)
            token, created = Token.objects.get_or_create(user=user)

            return Response({"token": token.key, "username": username}, status=200)
        except:
            return Response({"message": "Uneli ste pogrešan email i/ili lozinku."}, status=404)

class LogoutView(APIView):
    authentication_classes = (TokenAuthentication, )

    def post(self, request):
        try:
            token = request.META.get('HTTP_TOKEN')
            django_logout(request)
            #NOTE: better for multiple session status
            #Token.objects.get(key = token).delete()
            return Response({"status":True}, status=204)
        except Exception as e:
            logging_exception(e)
            return Response({"status":False}, status=400)


class WorkerViewSet(viewsets.ModelViewSet):

    queryset = Worker.objects.all()
    serializer_class = WorkerSerializer
    authentication_classes = (TokenAuthentication,)
    lookup_field = 'id'


class ProjectViewSet(viewsets.ModelViewSet):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer
    authentication_classes = (TokenAuthentication,)
    lookup_field = 'id'

    def create(self, request):
        project = Project(project_name = request.data['project_name'], project_budget = \
        request.data['project_budget'])
        for (prop,value) in request.__dict__['_data'].items():
            if(value is not None and hasattr(project, prop)):
                if(prop == 'project_leader'):
                    value = get_object_or_404(ProenergyUser.objects, id = value)
                setattr(project, prop, value)
        project.save()

        return Response(self.serializer_class(project).data, 200)

    @action(methods=['get'], detail=True)
    def status(self, request, *args, **kwargs):
        project = self.get_object()
        project_report = ProjectReport(project)
        project_report.financialReport()

        return Response(project_report.financialReport(),200)

class WorkOrderViewSet(viewsets.ModelViewSet):

    queryset = WorkOrder.objects.all()
    serializer_class = WorkOrderSerializer
    authentication_classes = (TokenAuthentication,)
    lookup_field = 'id'

    def create(self, request):

        workOrder = WorkOrder()
        for (prop,value) in request.data.items():
            if(value is not None and hasattr(workOrder, prop)):
                if(prop == 'worker'):
                    value = get_object_or_404(Worker.objects, id = value)
                if(prop == 'project'):
                    value = get_object_or_404(Project.objects, id = value)
                setattr(workOrder, prop, value)

        workOrder.save()


        return Response(self.serializer_class(workOrder).data, 200)


    @action(methods=['post'], detail=True)
    def closeWorkOrder(self, request, *args, **kwarg):
        work_order = self.get_object()
        if(work_order.time_closed != None):
            return Response({'error':'Nalog je već zatvoren'}, 400)
        #print(timezone.now)
        work_order.time_closed = timezone.now()
        work_order.save()
        return Response(None,200)
    '''
    def list(self, request):
        #print(WorkOrder.ob)
        queryset = WorkOrder.objects.all().filter(id == 10)
        print(queryset)
        #serializer = self.serializer_class(queryset, many=True)
        #return Response(serializer.data)
        return Response(1)
        r#eturn WorkOrder.objects.filter(time_closed != None)
    '''

class PaymentViewSet(viewsets.ModelViewSet):

    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    authentication_classes = (TokenAuthentication,)
    lookup_field = 'id'

    def create(self, request):
        payment = Payment()

        for (prop,value) in request.data.items():
            if(value is not None and hasattr(payment, prop)):
                if(prop == 'project'):
                    value = get_object_or_404(Project.objects, id = value)
                    project = value
                if(prop == 'cost_type'):
                    value = get_object_or_404(CostType.objects, id = value)
                if(prop == 'payment_flag'):
                    flag = value
                if(prop == 'amount'):
                    amount = value
                setattr(payment, prop, value)


        '''
        if(project is not None):
            if(flag): project.project_income += amount
            else: project.project_costs += amount
        project.save()
        '''
        payment.save()

        return Response(self.serializer_class(payment).data, 200)




class WorkOrderReportViewSet(viewsets.ModelViewSet):
        queryset = WorkOrderReport.objects.all()
        serializer_class = WorkOrderReportSerializer
        authentication_classes = (TokenAuthentication,)
        lookup_field = 'id'

        def create(self, request):
            work_order_report = WorkOrderReport()
            print(hasattr(work_order_report, 'work_order_id'))
            for (prop,value) in request.data.items():
                print(prop, hasattr(work_order_report,prop))

                if(value is not None and hasattr(work_order_report, prop)):
                    if(prop == 'work_order'):
                        value = get_object_or_404(WorkOrder.objects, id = value)
                        #project = value
                    setattr(work_order_report, prop, value)

            work_order_report.save()
            return Response(self.serializer_class(work_order_report).data, 200)



class ReportViewSet(APIView):
    def get(self, request):

        project = worker = period = None
        proj_id = request.GET.get('project')
        worker_id = request.GET.get('worker')
        period = request.GET.get('period')
        if(proj_id and worker_id and period):
            return Response({'error':'Moguće je kombinovati projekat i radnika ili period'}, 400)

        if(period):
            print(period)
            resp = ProjectReport.periodicalReport(int(period))
        else:
            if(proj_id):
                project = Project.objects.get(id = proj_id)
            if(worker_id):
                worker = Worker.objects.get(id = worker_id)
            project_report = ProjectReport(project, worker)

            resp = project_report.projectReport()


        return Response(resp, 200 if 'error' not in resp else 400)


class CostTypeViewSet(viewsets.ModelViewSet):
    queryset = CostType.objects.all()
    serializer_class =  CostTypeSerializer
    authentication_classes = (TokenAuthentication,)
    lookup_field = 'id'


class ExcelReportView(APIView):

    def post(self, request):
        type = int(request.data['file_flag'])
        flag = ExcelService.import_from_excel(type)
        if(flag):
            resp = {"message":"Uspešno upisani podaci iz fajla."}
            status = 200
        else:
            resp = {"message":"Podaci nisu kompletno upisani. Proverite podatke u fajlu."}
            status = 400
        return Response(resp, status)
